package com.csnoozy.game.ui.skins

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Disposable
import ktx.assets.load
import ktx.inject.Context
import ktx.style.label
import ktx.style.skin
import ktx.style.tree

class DebugSkin(context: Context) : Disposable {
    private val assetManager: AssetManager = context.inject<AssetManager>().apply {
        load<Texture>("plus.png")
        load<Texture>("minus.png")
        load<BitmapFont>("mono.fnt")
        finishLoading()
    }

    val debugSkin = skin {
        tree {
            plus = SpriteDrawable(Sprite(assetManager.get<Texture>("plus.png")))
            minus = SpriteDrawable(Sprite(assetManager.get<Texture>("minus.png")))
        }
        label {
            font = assetManager.get<BitmapFont>("mono.fnt")
            fontColor = Color.CORAL
        }
    }

    override fun dispose() {
        assetManager.apply {
            unload("plus.pmg")
            unload("minus.png")
            unload("mono.fnt")
        }
    }
}