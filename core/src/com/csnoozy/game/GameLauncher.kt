package com.csnoozy.game

import com.badlogic.gdx.Screen
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.math.RandomXS128
import com.badlogic.gdx.physics.box2d.Box2D
import com.csnoozy.game.screens.ExampleScreen
import ktx.app.KtxGame
import ktx.assets.load
import ktx.inject.Context

class GameLauncher : KtxGame<Screen>() {

    override fun create() {

        Box2D.init()

        val context = Context().apply {
            register {
                bindSingleton { AssetManager()}
                bindSingleton { RandomXS128(0L) }
            }
        }

        addScreen(ExampleScreen(context))
        setScreen<ExampleScreen>()
    }
}


