package com.csnoozy.game.camera

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import com.csnoozy.game.player.Player
import ktx.inject.Context


class Camera2d(private val context: Context) {

    private val player: Player = context.inject()

    var scale: Float = 1f

    val camera = OrthographicCamera().apply {
        setToOrtho(false, 30f * scale, 20f * scale)
        update()
    }

    fun zoom(change: Float) {
        scale += change
        scale = MathUtils.clamp(scale, 0.1f, 5f)
        camera.setToOrtho(false, 30f * scale, 20f * scale)
        camera.update()
    }

    fun update() {
        camera.position.set(player.body.position, camera.position.z)
        camera.update()
    }

    fun resize(width: Int, height: Int) {
        camera.setToOrtho(false, 30f * scale, 20f * scale)
        camera.update()
    }
}