package com.csnoozy.game.player

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.csnoozy.game.filter.LIGHT_GROUP
import com.csnoozy.game.filter.MASK_PICKUP
import com.csnoozy.game.filter.MASK_PLAYER
import com.csnoozy.game.filter.PLAYER
import ktx.assets.load
import ktx.box2d.filter
import ktx.inject.Context

private const val SPEED = 2f

class Player(context: Context) {

    var up = false
    var down = false
    var right = false
    var left = false

    private val assetManager: AssetManager = context.inject<AssetManager>().apply {
        load<Texture>("player.png")
        finishLoading()
    }
    private val world: World = context.inject()
    private val batch: SpriteBatch = context.inject()

    private val sprite: Sprite = Sprite(assetManager.get<Texture>("player.png")).apply {
        setSize(1f, 1f)
    }

    val body: Body = world.createBody(BodyDef().apply {
        type = BodyDef.BodyType.DynamicBody
        position.set(5f, 5f)
    })

    val fixture = body.createFixture(FixtureDef().apply {
        shape = CircleShape().apply {
            radius = 0.5f
        }
        density = 1f
        friction = 20f
        filter {
            categoryBits = PLAYER
            groupIndex = LIGHT_GROUP
            maskBits = MASK_PLAYER
        }
    })

    fun render(delta: Float) {
        body.applyLinearImpulse(
                Vector2(
                        if (left && !right) -SPEED
                        else (if (right && !left) SPEED else 0f),
                        if (down && !up) -SPEED
                        else (if (up && !down) SPEED else 0f)),
                body.position,
                true)

        body.applyForce(body.linearVelocity.lerp(Vector2(0f, 0f), 10f), body.position, true)
        sprite.setPosition(body.position.x - sprite.width / 2, body.position.y - sprite.height / 2)
        sprite.draw(batch)
    }
}