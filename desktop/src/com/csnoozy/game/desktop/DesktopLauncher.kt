package com.csnoozy.game.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.csnoozy.game.GameLauncher

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.apply {
            width = 1920
            height = 1080
            fullscreen = false
            vSyncEnabled = true
        }
        LwjglApplication(GameLauncher(), config)
    }
}